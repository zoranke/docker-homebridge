FROM node:8.9
MAINTAINER Dmitry Kireev <dmitry@kireev.co>
##################################################
# Set environment variables                      #
##################################################

ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
##################################################
# Install tools                                  #
##################################################

RUN apt-get update && \
    apt-get install -y \
    apt-utils \
    apt-transport-https \
    locales \
    curl \
    wget \
    git \
    python \
    build-essential \
    make \
    g++ \
    libavahi-compat-libdnssd-dev \
    libbluetooth-dev \
    libcap2-bin \
    libkrb5-dev \
    net-tools \
    nano

##################################################
# Install homebridge                             #
##################################################

RUN npm install -g homebridge \
    homebridge-config-ui \
    homebridge-mi-aqara \
    homebridge-homeassistant \
    homebridge-broadlink-rm \
    homebridge-mi-air-purifier miio \
    homebridge-xiaomi-mi-robot-vacuum \
    homebridge-camera-ffmpeg \
    --unsafe-perm

##################################################
# Start                                          #
#################################################

USER root
RUN mkdir -p /var/run/dbus

ADD start.sh /root/start.sh

VOLUME /root/.homebridge
EXPOSE 5353 51826
CMD ["/root/start.sh"]
